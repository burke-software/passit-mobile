// register polyfill-crypto component
import 'nativescript-angular-webview-crypto';

// add fake global crypto, so that import time `randombytes` tests for crypto
// will pass. Once the polyfill-crypto component is intialized
// this will be overriden by the real crypto.
global.crypto = {
    getRandomValues: _ => _
}