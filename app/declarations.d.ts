// These only exist on the web - but we want tsc to shut the fuck up about them
declare module "angular2-select";
declare module "ng-inline-svg";
declare module "ngx-clipboard";
declare module "ngx-clipboard/src/clipboard.directive";
declare module "CopySecretDirective"
declare module "clipboard";

interface ObjectConstructor {
    assign(target: any, ...sources: any[]): any;
    is(value1: any, value2: any): boolean;
    setPrototypeOf(o: any, proto: any): any;
}

declare interface Array<T> {
  find(predicate: (element: T, index?: number, array?: Array<T>) => boolean): T;
  findIndex(predicate: (element: T, index?: number, array?: Array<T>) => boolean): number;
}
