# Passit mobile

1. Start genymotion
2. `tns run android`

## Hacks

1. a bunch of package.json postinstall scripts to add polyfills for crypto things
2. some other global polyfill aliases in `./app/*-polyfill.ts`.
3. Changed the API URL in `./frontend/ngsdk/api.ts` to be the genymotionb localhost URL