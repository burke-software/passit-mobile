import { LoginContainer } from "passit-frontend/dist/out-tsc/app//user/login/login.container";

export const routes = [
    { path: "login", component: LoginContainer },
];