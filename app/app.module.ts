import { NgModule } from "@angular/core";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import './btoa-polyfill';
import './text-encoding-polyfill';
import './crypto-polyfill';

import Api from "passit-frontend/dist/out-tsc/app/ngsdk/api";
import NgPassitSDK from "passit-frontend/dist/out-tsc/app/ngsdk/sdk";
import { UserService } from "passit-frontend/dist/out-tsc/app/shared";
import { HttpModule } from "@angular/http";
import { StoreModule } from "@ngrx/store";
import { StoreDevtoolsModule } from "@ngrx/store-devtools";

// to register `polyfill-crypto` component
import "nativescript-angular-webview-crypto";

import { PassitApp } from "./app.component";
import { routes } from "./app.routing";
import { LoginContainer } from "passit-frontend/dist/out-tsc/app/user/login/login.container";
import BaseURL from "passit-frontend/dist/out-tsc/app/ngsdk/base-url";
import { LoginComponent } from "./user";
import { reducer } from "passit-frontend/dist/out-tsc/app/app.reducers";





@NgModule({
  declarations: [LoginContainer, PassitApp, LoginComponent],
  bootstrap: [PassitApp],
  entryComponents: [LoginContainer],
  imports: [
    NativeScriptRouterModule,
    NativeScriptRouterModule.forRoot(routes),
    HttpModule,
    StoreModule.provideStore(reducer),
    StoreDevtoolsModule.instrumentOnlyWithExtension(),
  ],
  providers: [
    { provide: Api, useClass: Api },
    { provide: BaseURL, useValue: "http://10.0.3.2:8000/api/"},
    { provide: NgPassitSDK, useClass: NgPassitSDK},
    { provide: UserService, useClass: UserService},
  ]
})
export class AppModule { }