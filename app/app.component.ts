import { Component } from "@angular/core";

@Component({
    selector: 'passit-app',
    template: `
        <StackLayout>
            <polyfill-crypto></polyfill-crypto>
            <StackLayout class="nav">
                <Button text="First" 
                    [nsRouterLink]="['/login']"></Button>
              </StackLayout>
            <router-outlet></router-outlet>
        </StackLayout>
    `
})
export class PassitApp {
}